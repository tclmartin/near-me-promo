(function () {
	var userLocation = {

		cities: [
			["city1", 10, 50, "blah"],
			["city2", 40, 60, "blah"],
			["city3", 25, 10, "blah"],
			["city4", 5, 80, "blah"]
		],
		// Callback function for asynchronous call to HTML5 geolocation
		init: function () {
			this.cacheDom();
			navigator.geolocation.getCurrentPosition(this.getLocation);
		},

		cacheDom: function () {
			this.$el = $('#promoControls');
			this.$latitude = this.$el.find('.latitude');
			this.$longitude = this.$el.find('.longitude');
			this.$triggerRadius = this.$el.find('.triggerRadius');
			this.$promoText = this.$el.find('.promoText');

		},
		nearestCity: function (latitude, longitude) {
			var mindif = 99999;
			var closest;

			for (index = 0; index < this.cities.length; ++index) {
				var dif = this.pythagorasEquirectangular(latitude, longitude, this.cities[index][1], this.cities[index][2]);
				if (dif < mindif) {
					closest = index;
					mindif = dif;
				}
			}

			// echo the nearest city
			alert(cities[closest]);
		},
		getLocation: function (position) {
			console.log(this.$el);
			this.nearestCity(position.coords.latitude, position.coords.longitude);
		},
		deg2Rad: function (deg) {
			return deg * Math.PI / 180;
		},
		pythagorasEquirectangular: function (lat1, lon1, lat2, lon2) {
			lat1 = this.deg2Rad(lat1);
			lat2 = this.deg2Rad(lat2);
			lon1 = this.deg2Rad(lon1);
			lon2 = this.deg2Rad(lon2);
			var R = 6371; // km
			var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
			var y = (lat2 - lat1);
			var d = Math.sqrt(x * x + y * y) * R;
			return d;
		},

	};


	userLocation.init();


})();