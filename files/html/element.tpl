<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC67buWa4EcINgnnzxadU6lOCaKQV8FACc">
</script>
{{#activeState}}
    <style>
        .js-display-none{
            display: none;
        }
        .promo-controls.p-fixed{
            position: fixed;
            top: 0;
            width: 100%;
            z-index: 800;
        }
        .calloutBlock{
            width: 100%;
            text-align: center;
            padding: 5px;
            box-sizing: border-box;
            margin-bottom: 10px;
            min-height: 25px;
            position: relative;
        }
        .calloutBlock .callout-close{
            position: absolute;
            top:20%;
            right: 1%;
            cursor: pointer;
        }
    </style>
    <div id="visitorsNearby" class="promo-controls js-display-none">
        <input type="hidden" name="latitude" class="latitude" value="{{latitude}}">
        <input type="hidden" name="longitude" class="longitude" value="{{longitude}}">
        <input type="hidden" name="radiusType" class="radiusType" value="{{radiusType}}">
        <input type="hidden" name="radius" class="radius" value="{{radius}}">
        <div id="sitePromo" class="calloutBlock">
            <p class="callout">
                {{promoText}} {{#promoLink}}<a class="callout-link" data-link="{{promoLink}}">{{promoLinkName}}</a>{{/promoLink}}
            </p>
            <i class="callout-close fa fa-close js-close-promo"></i>
        </div>
    </div>
{{/activeState}}