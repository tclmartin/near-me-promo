/**
 * This is required for element rendering to be possible
 * @type {PlatformElement}
 */
(function(){
	"use strict";

	var userLocation,
		visitorsNearby;

	visitorsNearby = {

		/**
		 * our init for promo location which assigns local vars and caches them
		 * @return undefined
		 */
		init: function(){
			this.cacheDom();
			this.bindEvents();
		},

		/**
		 * bind events
		 * @return undefind
		 */
		bindEvents: function() {
			var that = this;
			$(window).on('scroll', function (event) {
				var height = $(window).scrollTop();
				if(height > 300) {
					that.$el.addClass('p-fixed');
				} else if (height < 300) {
					that.$el.removeClass('p-fixed');
				}
			});

			this.$el.on('click', '.promo-close', function () {
				this.remove();
			}.bind(this));
		},

		/**
		 * cache or dom elements we are going to use
		 * @return undefined
		 */
		cacheDom: function() {
			this.$el = $('#visitorsNearby');
			this.$mainContainer = this.$el.closest('.platform-element-contents');
			this.promoLatitude = Number(this.$el.find('.latitude').val());
			this.promoLongitude = Number(this.$el.find('.longitude').val());
			this.userLatitude = '';
			this.userLongitude = '';
			this.userCountryId = null;
			this.promoCountryId = null;
			this.checkCountry = false;
			this.radius = this.$el.find('.radius').val();
			this.radiusType = this.$el.find('.radiusType').val();
			this.link = this.$el.find('.callout-link').data('link');
		},

		/**
		 * get the country Id from google maps geocoder using lat and long
		 * @param lat
		 * @param lng
		 * @return string
		 */
		getCountryId: function (lat, lng, handle) {
			var geocoder= new google.maps.Geocoder(),
				latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
			geocoder.geocode({'location': latlng}, function(results, status) {
				var country;
				if (status === 'OK') {
					if (results[1]) {
						for (var i=0; i<results[0].address_components.length; i++) {
							for (var b=0;b<results[0].address_components[i].types.length;b++) {
								if (results[0].address_components[i].types[b] == "country") {
									//this is the object you are looking for
									country = results[0].address_components[i];
									break;
								}
							}
						}
						this.setCounrtyData(country.short_name, handle);
					} else {
						console.log('No results found');
					}
				} else {
					console.log('Geocoder failed due to: ' + status);
				}
			}.bind(this));


		},

		/**
		 * Is the user in the radius set in the app
		 * @param latitude
		 * @param longitude
		 * @return bool
		 */
		isUserNearPromo: function() {
			if(this.getCheckCountry() && this.getPromoCountryId() === this.getUserCountryId()) {
				this.render();
				return;
			}
			var dif = this.radiusCheck(this.getUserLatitude(), this.getUserLongitude(), this.getPromoLatitude(), this.getPromoLongitude());
			if (dif < this.radius) {
				this.render();
			}
		},

		/**
		 * Some Math That I use to know when I was a cad tech
		 * but dont remember
		 * @param deg
		 * @return int
		 */
		deg2Rad: function(deg) {
			return deg * Math.PI / 180;
		},

		/**
		 * look for
		 * @param lat1
		 * @param lon1
		 * @param lat2
		 * @param lon2
		 * @return {number}
		 */
		radiusCheck: function(lat1, lon1, lat2, lon2) {
			//convert Miles to Kilometers
			lat1 = this.deg2Rad(lat1);
			lat2 = this.deg2Rad(lat2);
			lon1 = this.deg2Rad(lon1);
			lon2 = this.deg2Rad(lon2);
			var R = 6371,
				x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2),
				y = (lat2 - lat1),
				d = Math.sqrt(x * x + y * y) * R;
			return d;
		},

		/**
		 * render our template on the dom
		 * @return undefined
		 */
		render: function() {
			this.$el.removeClass('js-display-none');
			this.$el.find('.callout');
			this.buildLinkProtocol();
			$('body').prepend(this.$mainContainer);
		},

		/**
		 * hide our element
		 * @return undefined
		 */
		remove: function() {
			this.$el.addClass('js-display-none');
		},
		/**
		 * remove protocol and append link to href
		 *@return undefined
		 */
		buildLinkProtocol: function () {

			var link = this.getLink().replace(/.*?:\/\//g, "http://");
			// check before adding protocol if on same domain
			// if not same doamin add attr target blank
			this.$el.find('.callout-link').attr('href', link);
		},

		/**
		 * set the user latitude
		 * @param int
		 * @return undefined
		 */
		setUserLatitude: function (latitude) {
			this.userLatitude = latitude;
		},

		/**
		 * set the user longitude
		 * @param int
		 * @return undefined
		 */
		setUserLongitude: function (longitude) {
			this.userLongitude = longitude;
		},

		/**
		 * set the country data
		 * @param data from geocoder
		 * @param handler
		 * @return undefined
		 */
		setCounrtyData: function (data, handle) {
			switch(handle) {
				case 'user':
					this.setUserCountryId(data);
					break;
				case 'promo':
				default:
					this.setPromoCountryId(data);
					break;
			}

			if(this.getPromoCountryId() && this.getUserCountryId()) {
				this.isUserNearPromo();
			}
		},

		/**
		 * set user country id
		 * @param data string
		 * @return undefined
		 */
		setUserCountryId: function (data) {

			this.userCountryId = data;
		},

		/**
		 * set promo country id
		 * @param data string
		 * @return undefined
		 */
		setPromoCountryId: function (data) {
			this.promoCountryId = data;
		},

		/**
		 * set the flag to use country as the check instead of the radius
		 * @param bool
		 * @return undefined
		 */
		setCheckCountry: function (status) {
			this.checkCountry = status;
		},

		/**
		 * set the radius check
		 * @return undefined
		 */
		setRadiusCheck: function () {
			switch (this.getRadiusType()) {
				case 'Same Country':
					this.setCheckCountry(true);
					break;
				case 'Miles':
					this.setRadiusToMiles();
					break;
				case 'Kilometers':
				default:
					break;
			}
		},
		/**
		 * set our radius to miles which converts to kilometers
		 * @return undefined
		 */
		setRadiusToMiles: function () {
			return this.getRadius() * 1.6;
		},

		/**
		 * get the promo link
		 * @return string
		 */
		getLink: function () {
			return this.link;
		},
		/**
		 * get the user latitude
		 * @return int
		 */
		getUserLatitude: function () {
			return this.userLatitude;
		},

		/**
		 * get the user longitude
		 * @return int
		 */
		getUserLongitude: function () {
			return this.userLongitude;
		},

		/**
		 * get the user country code
		 * @return string
		 */
		getUserCountryId: function () {
			return this.userCountryId;
		},

		/**
		 * get the promo latitude
		 * @return int
		 */
		getPromoLatitude: function () {
			return this.promoLatitude;
		},

		/**
		 * get the promo longitude
		 * @return int
		 */
		getPromoLongitude: function () {
			return this.promoLongitude;
		},

		/**
		 * get promo country that was set in weebly admin
		 * @return string
		 */
		getPromoCountryId: function () {
			return this.promoCountryId;
		},

		/**
		 * get the status of the checkCountry flag
		 * @return bool
		 */
		getCheckCountry: function () {
			return this.checkCountry;
		},

		/**
		 * strip text from string and return a number
		 * @param radius
		 * @return number
		 */
		getRadius: function() {
			return this.radius;
		},

		/**
		 * get the radius type
		 * @return undefined
		 */
		getRadiusType: function() {
			return this.radiusType;
		},

	};

	userLocation = {
		/**
		 * Make ajax call to geo location to location
		 * @return undefined
		 */
		init: function() {
			var that = this,
				request = new XMLHttpRequest();
			request.open('POST', 'https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyCVgzJFTJrzGjSKFjc7_hbLxRZeaOlMZdA', true);
			request.send();
			request.onload = function() {
				if (request.status >= 200 && request.status < 400) {
					// Success!
					var data = JSON.parse(request.responseText);
					that.getLocation(data);
				} else {
					// We reached our target server, but it returned an error
					console.log('request failed');

				}
			};
			request.onerror = function(errorThrown) {
				// There was a connection error of some sort
				console.log('request failed');
			};

		},

		/**
		 * call back for get current location and call or man module
		 * @param position
		 */
		getLocation: function(position) {
			visitorsNearby.init();
			visitorsNearby.setRadiusCheck();
			visitorsNearby.getCountryId(visitorsNearby.getPromoLatitude(), visitorsNearby.getPromoLongitude(), 'promo');
			visitorsNearby.setUserLatitude(position.location.lat);
			visitorsNearby.setUserLongitude(position.location.lng);
			visitorsNearby.getCountryId(visitorsNearby.getUserLatitude(), visitorsNearby.getUserLongitude(), 'user');

		}
	};


	if(!$('body').hasClass('wsite-editor')) {
		userLocation.init();
	}

	var MyElement = PlatformElement.extend({});

	return MyElement;
})();

